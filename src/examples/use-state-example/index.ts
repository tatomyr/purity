import {init} from '../../purity.js'
import {Root} from './Root.js'

export const {mount, rerender} = init({})

mount(Root)
