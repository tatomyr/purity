import {groomTasks} from './tasks.js'

export const startup = (): void => {
  groomTasks()
}
